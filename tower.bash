#!/bin/bash -ex

#host="https://94d9ce5357d7.ngrok.io"
#username="admin"
#password="redhat"
#ID=9

echo "Configuring Tower Settings"
hostval=$(tower-cli config host $host)
userval=$(tower-cli config username $username)
passwordval=$(tower-cli config password $password)

if [[ $userval == "username: " ]] || [[ $passwordval == "password: " ]]
then
  echo "WARNING: Configuration has not been fully set";
  echo "   You will want to run the $ tower-cli config ";
  echo "   command for host, username, and password ";
fi

echo " current configuration settings:"
echo $hostval
echo $userval
echo $passwordval

tower-cli config verify_ssl false

# Let's run a tower-cli job
tower-cli job launch --job-template $ID --monitor
